package com.daict.pay.service;

import com.daict.pay.bo.UserEntity;
import com.daict.pay.constant.CommonUtils;
import com.daict.pay.constant.ErrorEnum;
import com.daict.pay.repository.UserRepository;
import com.daict.pay.request.LoginRequest;
import com.daict.pay.response.BaseResponse;
import com.daict.pay.response.UserInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
@AllArgsConstructor
public class HomeService {
    private ObjectMapper mapper;
    private UserRepository userRepository;
    public BaseResponse<UserInfo> login(LoginRequest request) {
        BaseResponse<UserInfo> response = new BaseResponse<>();
        try {
            log.info("BEGIN login, request={}", mapper.writeValueAsString(request));
            String password = CommonUtils.Sha256(request.getPassword());
            UserEntity entity = userRepository.findByUserName(request.getUserName());
            if (entity.getPassWord().equals(password)) {
                log.info("login success!");
                UserInfo userInfo = new UserInfo();
                userInfo.setUserName(request.getUserName());
                userInfo.setRole(entity.getRole());
                response.setData(userInfo);
            } else {
                log.info("login false!");
            }
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.SUCCESS.getCode());
            response.setMessage(ErrorEnum.SUCCESS.getMessage());
        } catch (Exception ex) {
            log.error("EXCEPTION login. ex=", ex);
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.EXCEPTION.getCode());
            response.setMessage(ErrorEnum.EXCEPTION.getMessage());
        }
        log.info("END login");
        return response;
    }
}
