package com.daict.pay.service;

import com.daict.pay.bo.BillEntity;
import com.daict.pay.constant.ErrorEnum;
import com.daict.pay.repository.BillRepository;
import com.daict.pay.response.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class BillService {
    private BillRepository billRepository;

    public List<BillEntity> findAll() {
        return billRepository.findAll();
    }

    public BaseResponse<BillEntity> findById(String billId) {
        BaseResponse<BillEntity> response = new BaseResponse<>();
        try {
            BillEntity entity = billRepository.findByBillId(billId);
            response.setData(entity);
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.SUCCESS.getCode());
            response.setMessage(ErrorEnum.SUCCESS.getMessage());
        } catch (Exception ex) {
            log.error("EXCEPTION get bill detail, ex=", ex);
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.EXCEPTION.getCode());
            response.setMessage(ErrorEnum.EXCEPTION.getMessage());
        }
        return response;
    }
}
