package com.daict.pay.service;

import com.daict.pay.bo.UserEntity;
import com.daict.pay.constant.CommonUtils;
import com.daict.pay.constant.ErrorEnum;
import com.daict.pay.constant.RoleEnum;
import com.daict.pay.repository.UserRepository;
import com.daict.pay.request.AddUserRequest;
import com.daict.pay.request.EditUserRequest;
import com.daict.pay.response.BaseResponse;
import com.daict.pay.response.UserInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class UserService {
    private UserRepository userRepository;
    private ObjectMapper mapper;
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    public BaseResponse<UserInfo> processAddUser(AddUserRequest request) {
        BaseResponse<UserInfo> response = new BaseResponse<>();
        try {
            log.info("BEGIN add user. request={}", mapper.writeValueAsString(request));
            // check duplicate
            if (checkUserExisted(request.getUserName())) {
                response.setResponseId(String.valueOf(new Date().getTime()));
                response.setCode(ErrorEnum.DUPLICATE.getCode());
                response.setMessage(ErrorEnum.DUPLICATE.getMessage());
                return response;
            }
            UserEntity entity = new UserEntity();
            entity.setUserName(request.getUserName());
            entity.setPassWord(CommonUtils.Sha256(request.getPassword()));
            entity.setRole(RoleEnum.getRoleByCode(request.getRole()).getCode());
            entity.setCreatedDate(new Date());
            userRepository.save(entity);
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.SUCCESS.getCode());
            response.setMessage(ErrorEnum.SUCCESS.getMessage());
            UserInfo userInfo = new UserInfo();
            userInfo.setUserName(entity.getUserName());
            userInfo.setRole(entity.getRole());
            userInfo.setCreatedDate(entity.getCreatedDate());
            response.setData(userInfo);
        } catch (Exception ex) {
            log.error("Exception add user.");
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.EXCEPTION.getCode());
            response.setMessage(ErrorEnum.EXCEPTION.getMessage());
        }
        log.info("END add user");
        return response;
    }

    public BaseResponse<UserInfo> processEditUser(EditUserRequest request) {
        BaseResponse<UserInfo> response = new BaseResponse<>();
        try {
            log.info("BEGIN edit user. request={}", mapper.writeValueAsString(request));
            // check exist
            UserEntity entity = userRepository.findByUserName(request.getUserName());
            if (entity == null) {
                log.error("User not exist.");
                response.setResponseId(String.valueOf(new Date().getTime()));
                response.setCode(ErrorEnum.NOT_EXIST.getCode());
                response.setMessage(ErrorEnum.NOT_EXIST.getMessage());
                return response;
            }
            // save
            entity.setRole(request.getRole());
            userRepository.save(entity);
            // build response
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.SUCCESS.getCode());
            response.setMessage(ErrorEnum.SUCCESS.getMessage());
            UserInfo userInfo = new UserInfo();
            userInfo.setUserName(entity.getUserName());
            userInfo.setRole(entity.getRole());
            userInfo.setCreatedDate(entity.getCreatedDate());
            response.setData(userInfo);
        } catch (Exception ex) {
            log.error("Exception add user.");
            response.setResponseId(String.valueOf(new Date().getTime()));
            response.setCode(ErrorEnum.EXCEPTION.getCode());
            response.setMessage(ErrorEnum.EXCEPTION.getMessage());
        }
        log.info("END edit user");
        return response;
    }

    private boolean checkUserExisted(String userName) {
        UserEntity entity = userRepository.findByUserName(userName);
        return entity != null;
    }
}
