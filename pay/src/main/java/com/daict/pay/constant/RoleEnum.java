package com.daict.pay.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RoleEnum {
    ADMIN("1", "Quyền admin"),
    USER("2", "Quyền user"),
    UN_KNOW("3", "Không xác định"),;
    private String code;
    private String description;

    public static RoleEnum getRoleByCode(String code) {
        for (RoleEnum roleEnum : RoleEnum.values()) {
            if (roleEnum.getCode().equals(code)) {
                return roleEnum;
            }
        }
        return UN_KNOW;
    }
}
