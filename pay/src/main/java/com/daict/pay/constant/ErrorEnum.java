package com.daict.pay.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorEnum {
    SUCCESS("00", "SUCCESS"),
    NOT_EXIST("02", "NOT EXIST"),
    ERROR("03", "ERROR"),
    DUPLICATE("05", "DUPLICATE"),
    TIME_OUT("98", "TIME OUT"),
    EXCEPTION("99", "EXCEPTION");
    private String code;
    private String message;
}
