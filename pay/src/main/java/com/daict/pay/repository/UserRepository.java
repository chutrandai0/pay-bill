package com.daict.pay.repository;

import com.daict.pay.bo.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    List<UserEntity> findAll();
    UserEntity findByUserName(String userName);
}
