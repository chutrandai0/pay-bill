package com.daict.pay.repository;

import com.daict.pay.bo.BillEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BillRepository extends JpaRepository<BillEntity, String> {
    List<BillEntity> findAll();
    BillEntity findByBillId(String billId);
}
