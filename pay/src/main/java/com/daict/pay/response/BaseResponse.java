package com.daict.pay.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseResponse<T> {
    private String responseId;
    private String code;
    private String message;
    private T data;
}
