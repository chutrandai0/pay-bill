package com.daict.pay.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserInfo {
    private String userName;
    private String role;
    private Date createdDate;
}
