package com.daict.pay.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddUserRequest {
    private String requestId;
    private String userName;
    private String password;
    private String role;
}
