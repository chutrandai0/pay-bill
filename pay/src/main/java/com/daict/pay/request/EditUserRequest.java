package com.daict.pay.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditUserRequest {
    private String requestId;
    private String userName;
    private String role;
}
