package com.daict.pay.controller;

import com.daict.pay.bo.BillEntity;
import com.daict.pay.response.BaseResponse;
import com.daict.pay.service.BillService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.MDC;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "bill")
@AllArgsConstructor
public class BillController {
    private BillService billService;
    private ObjectMapper mapper;


    @GetMapping(value = "list")
    public String getBillList() throws JsonProcessingException {
        List<BillEntity> lst = billService.findAll();
        return mapper.writeValueAsString(lst);
    }

    @GetMapping(value = "detail/{billId}")
    public BaseResponse<BillEntity> getBillDetail(@Param("billId") String billId) {
        MDC.put("token", billId);
        return billService.findById(billId);
    }

    // TODO create bill
    // TODO process pay bill
    // TODO ipn
}
