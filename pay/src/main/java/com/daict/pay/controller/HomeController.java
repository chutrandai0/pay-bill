package com.daict.pay.controller;

import com.daict.pay.request.LoginRequest;
import com.daict.pay.response.BaseResponse;
import com.daict.pay.response.UserInfo;
import com.daict.pay.service.HomeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "home")
@AllArgsConstructor
@Slf4j
public class HomeController {
    private HomeService homeService;

    @GetMapping("ping")
    public String ping() {
        MDC.put("token", "pong");
        log.info("ponggggggggggggg");
        return "pong";
    }

    @PostMapping("login")
    @ResponseBody
    public BaseResponse<UserInfo> login(@Valid @RequestBody LoginRequest request) {
        MDC.put("token", request.getRequestId());
        return homeService.login(request);
    }

    // TODO Forgot PW
}
