package com.daict.pay.controller;

import com.daict.pay.bo.UserEntity;
import com.daict.pay.request.AddUserRequest;
import com.daict.pay.request.EditUserRequest;
import com.daict.pay.response.BaseResponse;
import com.daict.pay.response.UserInfo;
import com.daict.pay.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "user")
@AllArgsConstructor
public class UserController {
    private UserService userService;
    private ObjectMapper mapper;

    @GetMapping("/list")
    public String getUserList() throws JsonProcessingException {
        List<UserEntity> lst = userService.findAll();
        return mapper.writeValueAsString(lst);
    }

    @PostMapping("/add")
    @ResponseBody
    public BaseResponse<UserInfo> processAddUser(@RequestBody @Valid AddUserRequest request) {
        MDC.put("token", request.getRequestId());
        return userService.processAddUser(request);
    }

    @PostMapping("/edit")
    @ResponseBody
    public BaseResponse<UserInfo> processEditUser(@RequestBody @Valid EditUserRequest request) {
        MDC.put("token", request.getRequestId());
        return userService.processEditUser(request);
    }

    // TODO LOCK/UN_LOCK
}
