package com.daict.pay.bo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "BILL")
@Getter
@Setter
public class BillEntity {
    @Id
    @Column(name = "bill_id")
    private String billId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "status")
    private String status;
    @Column(name = "description")
    private String description;
    @Column(name = "created_date")
    private Date createdDate;

}
