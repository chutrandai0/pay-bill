package com.daict.pay.bo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "USER")
@Getter
@Setter
public class UserEntity {
    @Id
    @Column(name = "user_name")
    private String userName;
    @Column(name = "pass_word")
    private String passWord;
    @Column(name = "role")
    private String role;
    @Column(name = "created_date")
    private Date createdDate;
}
